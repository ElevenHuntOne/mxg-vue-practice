# mxg-msm 梦学谷vue学习记录——管理系统。

> ### 去[后端](https://gitee.com/ElevenHuntOne/mxg-springboot-practice)

### [Demo](http://vue.xiaoao.space) | 账号：gmfll | 密码：12345

宣传用图片：

![mxg-vue-practice](https://socialify.git.ci/ZhangAo1999/mxg-vue-practice/image?description=1&font=Rokkitt&forks=1&issues=1&language=1&logo=https%3A%2F%2Fedu-image.nosdn.127.net%2Fa80cfb21-a1b3-4e40-a0da-b164452032f9.png&owner=1&pattern=Plus&pulls=1&stargazers=1&theme=Light)

分享用二维码：

![](./public/gitee仓库.png)

![](https://img.shields.io/badge/Node.js-10.12.0-blue)
![](https://img.shields.io/badge/Vue-2.6.11-blue)
![](https://img.shields.io/badge/ElementUI-2.14.0-blue)

## 使用说明

下载后`cd mxg-vue-practice`，先运行`npm install`，然后运行`npm run serve`就可以正常访问啦！

注：因为是只有Vue + Element UI，没有后端，所以api接口都是使用的我在梦学谷搭建的那个easy-mock网址上定义的。如果你觉得太卡了，经常访问超时的话，可以按照我在本项目所需的所有API目录下的提示创建出一套属于自己的api接口。接口的文档我全部提供了，非常完整！（并且在他原视频的基础上加了一些判断，比如密码必须为123啊，查询的时候会根据页数返回id啊之类的。）

## 如何部署到nginx服务器？（后端开发已完成！[点我跳转](https://gitee.com/ElevenHuntOne/mxg-springboot-practice)）

* `npm run build` 将代码打包。（确定是已经`npm install`过的！）

* 可以把生成的`dist`目录重命名成`mxg-mms`，或者其他任何名字

* 以上两步也可以通过下载我的[发行版](https://gitee.com/ElevenHuntOne/mxg-vue-practice/releases/vuex-complete-v1.1)解决。

* 上传到服务器（或虚拟机）

* 在服务器（或虚拟机）的nginx配置文件中添加下方配置
```nginx
server
    {
        listen 80;
        server_name vue.xiaoao.space;# 请改成你的服务器的域名
        # include enable-php.conf;
        location /
        {
            root /www/wwwroot/mxg-mms;# 请改成你上传的位置
            index index.html index.htm index.php;
        }
        location '/pro-api'# 因为build后是生产环境了，所以要匹配pro-api的路径，同时`-`是个特殊字符，所以要用单引号引起来
        {
            # 代理转发到后台服务接口，请修改成你自己后端的地址。（这个是mock的前端测试接口，应该也能测试用吧？）需要注意的是，如果你是转发到http://xx.xx:8080啥的直接端口号的时候，要在后面加个/。我也不知道为啥，不加就是http://xx.xx:8080/pro-api/user/login，访问不了，加了就是http://xx.xx:8080//user/login，但是能访问了。。就听奇怪的。不知道是哪的原因。可能是nginx的锅吧。
            proxy_pass http://mengxuegu.com:7300/mock/5f9a6dd86fc18c2f51b559f9;
        }
        access_log  /www/wwwlogs/vue.xiaoao.space.log; # 输出错误日志的路径（我用的宝塔，他自动创建的www用户，所以，还需要上传之后把目录的所有者改成www，好像需要）
    }
```

* 最后，域名解析中加入一条vue.xiaoao.space A记录 服务器地址。（记得修改成自己的域名。。。）

## 登录页面

![](./public/登录页面.png)

## 导航菜单

![](./public/导航菜单.gif)

## 主体部分

### 分页处理

![](./public/分页处理.gif)

### logging加载

![](./public/logging加载.gif)

### 新增商品

![](./public/新增商品.gif)
