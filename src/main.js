import Vue from "vue";
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import App from "./App.vue";
import router from "./router";
import store from "./store";

Vue.use(ElementUI);

import './router/permission'

Vue.config.productionTip = process.env.NODE_ENV === 'production';
console.log(process.env.NODE_ENV);// 开发环境 development, 生产环境 production

new Vue({
  router,
  store, // 注册
  render: h => h(App)
}).$mount("#app");
